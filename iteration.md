# Iteration

Perulangan atau repetisi yang dapat dilakukan untuk mempermudah atau mempersingkat suatu proses.

Iterasi dapat dilakukan dengan menggunakan dua teknik, yaitu teknik looping (linear) dan rekursif.

Dalam C++, perulangan looping bisa menggunakan command sebagai berikut:

for loop
```cpp

for (<initialization>; <statement>; <counter>) {
  proses();
}

```

while loop
```cpp

<initialization>
while(<statement>) {
  proses();
  <counter>
}

```

do..while loop
```cpp
<initialization>
do {
  proses();
  <counter>
} while(<statement>);
```

## Going deeper with Looping

Secara konseptual, looping dapat dibagi menjadi 3 tahapan:
1. Initialization (dimulai dari berapa? dimulai dari kapan?)
2. Statement (sampai kapan looping akan berhenti?, looping dilakukan selama apa?)
3. Counter (improvement apa yang didapat setiap kamu melakukan looping?)

Counter Concept:
```
number = 1

number = number + 1 = 1 + 1 = 2
number = number + 1 = 2 + 1 = 3
number = number + 1 = 3 + 1 = 4

number = number + 1

number += 1

number++
```

Contoh: Print angka 1 hingga 3
```
Output: 1 2 3
```

Answer (for loop):
```
for(int number = 1; number <= 3; number++) {
  cout << number << " ";
}
```

Tahapan dalam for loop:
1. Initialization
   int number = 1

2. Repetition
   Repetition 1
   1. Check the statement (number <= 3)
      number = 1; number <= 3 is True (kondisi terpenuhi)
   2. Run the process
      cout << 1 << " "
   3. Run the counter (Make an improvement)
      number++ <=> number = number + 1 = 1 + 1 = 2
   
   Repetition 2
   1. Check the statement (number <= 3)
      number = 2; number <= 3 is True (kondisi terpenuhi)
   2. Run the process
      cout << 2 << " "
   3. Run the counter (Make an improvement)
      number++ <=> number = number + 1 = 2 + 1 = 3
  
   Repetition 3
   1. Check the statement (number <= 3)
      number = 3; number <= 3 is True (kondisi terpenuhi)
   2. Run the process
      cout << 3 << " "
   3. Run the counter (Make an improvement)
      number++ <=> number = number + 1 = 3 + 1 = 4

   Repetition 4
   1. Check the statement (number <= 3)
      number = 4; number <= 3 is False (kondisi tidak terpenuhi)
      Looping ends here.

Answer (while loop):
```
int number = 1;
while(number <= 3) {
  cout << number << " ";
  number++;
}

```

Tahapan proses dalam while loop sama dengan for loop

Answer (do..while loop)
```
int number = 1;
do {
  cout << number << " ";
  number++;
} while(number <= 3);
```

Tahapan proses dalam do..while loop:
1. Initialization
   int number = 1

2. Repetition
   Repetition 1
   1. Run the process
      cout << 1 << " "
   2. Run the counter
      number++ <=> number = number + 1 = 1 + 1 = 2
   3. Check the statement (number <= 3)
      number = 2; number <= 3 is True (kondisi terpenuhi)
    
   Repetition 2
   1. Run the process
      cout << 2 << " "
   2. Run the counter
      number++ <=> number = number + 1 = 2 + 1 = 3
   3. Check the statement (number <= 3)
      number = 3; number <= 3 is True (kondisi terpenuhi)
  
   Repetition 3
   1. Run the process
      cout << 3 << " "
   2. Run the counter
      number++ <=> number = number + 1 = 3 + 1 = 4
   3. Check the statement (number <= 3)
      number = 4; number <= 3 is False (kondisi tidak terpenuhi)
      Looping ends here
   

Rincian tahapan repetisi pada:
1. For loop
   Check the statement -> Run the process -> Run the counter

2. While loop
   Check the statement -> Run the counter -> Run the other process
   Check the statement -> Run the other process -> Run the counter

3. Do..while loop
   Run the counter -> Run the other process -> Check the statement
   Run the other process -> Run the counter -> Check the statement
