# Introduction

Covered Topics:
1. Data Type and Variables
2. Input and Output
3. Operator (Arithmetic, Boolean (Logic), Bitwise)
4. Flow Control (Selection and Iteration)
5. Pointer
6. Array
7. Function and Procedure

Algorithm:
1. Searching (Sequential Search)
2. Sorting (Bubble Sort, Insertion Sort)
3. Iteration -> Recursion
