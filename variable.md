# Variable

Tempat untuk menyimpan data dalam sebuah komputer sesuai dengan tipe data yang ditentukan.

# Intro

Terdapat 2 jenis variable dalam komputer:

1. Variable Biasa -> menyimpan data sesuai dengan tipe datanya.
2. Variable Pointer -> menyimpan alamat memori dari tempat penyimpan suata data. (Variable yang menyimpan address atau alamat dari suatu memori).

Syntax deklarasi variable pointer dalam C++:

```
<data-type> *<nama-variable>;
```

Contoh:

```cpp
#include <iostream>

using namespace std;

int main()
{
  int number = 10; // variable biasa
  int *number_address = &number; // variable pointer

  return 0;
}
```

Setiap variable memiliki alamat, sehingga variable pointer sendiri juga memiliki alamat.

Variable pointer hanya dapat menyimpan address memori dari suatu variable biasa yang memiliki tipe data yang sama dengan variable pointer.

Lambang dalam variable pointer:

- \*: Untuk mengetahui data yang tersimpan dalam suatu alamat (isi dari suatu alamat).
- &: Untuk mengetahui alamat dimana suatu data tersimpan.

## Code Snippet

```cpp
#include <iostream>

using namespace std;

int main()
{
  int number = 100;
  int *number_pointer = &number;

  cout << "VALUE inside NUMBER: " << number << "\n";
  cout << "NUMBER variable's ADDRESS: " << &number << "\n";
  cout << "VALUE inside NUMBER_POINTER: " << number_pointer << "\n";
  cout << "NUMBER_POINTER variable's ADDRESS: " << &number_pointer << "\n\n";

  cout << "VALUE of NUMBER access through NUMBER_POINTER: " << *number_pointer << "\n";
  cout << "VALUE of NUMBER access through NUMBER_POINTER's address: " << **(&number_pointer) << "\n";

  return 0;
}
```

```cpp
#include <iostream>

using namespace std;

int main()
{
  int number = 100;
  int *number_pointer = &number;
  
  cout << "Number: " << number << "\n";
  
  number = 90; // change the value of number through the variable itself
  cout << "Number: " << number << "\n";
  
  *number_pointer = 80; // change the number's value through it's address using pointer
  cout << "Number: " << number << "\n";

  return 0;
}
```