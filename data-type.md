# Data Type and Variables

## Intro

Variable -> tempat untuk menyimpan data.

Dalam C++, setiap variable harus memiliki tipe data untuk memberitahu komputer mengenai data apa yang dapat disimpan oleh variable tersebut.

Bagaimana cara memberitahukan komputer mengenai data apa yang dapat disimpan oleh variable?
Caranya adalah dengan melakukan **deklarasi variable**

Syntax C++ untuk melakukan deklarasi variable

```
<data-type> <variable-name>;
```

Contoh mendeklarasikan suatu variable dengan tipe data integer dalam C++

```cpp

#include <iostream>

using namespace std;

int main()
{
  int angka;

  return 0;
}

```


## Data Types

Dalam C++ terdapat beberapa jenis tipe data native (primitive):

- **Char**
  Tipe data karakter (terdapat 255 karakter dalam komputer). Ukuran dari char adalah 1 byte. Setiap karakter ditandai dengan menggunakan tanda petik satu.

- **Integer**
  Tipe data bilangan bulat (-2^15 s/d 2^15). Ukuran dari integer (int) adalah 4 byte.

- **Long Integer**
  Tipe data bilangan bulat (-2^31 s/d 2^31). Ukuran dari integer (int) adalah 8 byte.

- **Float**
  Tipe data bilangan pecahan. Ukuran dari float adalah 4 byte.

- **Double**
  Tipe data bilangan pecahan. Ukuran dari double adalah 8 byte.

## Utilize a Variable

Bagaimana cara menyimpan atau memasukkan data ke dalam sebuah variable?
Dilakukan proses **assignment**

```cpp

// Cara Pertama
int number;

number = 10;

// Cara Kedua
int number = 10;

```

## Code Snippet

```cpp
#include <iostream>

using namespace std;

int main()
{
    // declare a variable with integer data type
    int number = 10;

    // declare a variable with double as data type
    double decimal = 118347830.67;

    // declare a variable with char as data type
    char character = 'A';

    // declare a variable with float as data type
    float smaller_decimal = 10.67;


    cout << "Number: " << number << endl;
    cout << "Decimal: " << decimal << endl;
    cout << "Character: " << character << endl;
    cout << "Smaller Decimal: " << smaller_decimal << endl;

    return 0;
}
```
